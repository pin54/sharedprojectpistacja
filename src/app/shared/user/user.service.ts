import {InjectableCompiler} from '@angular/compiler/src/injectable_compiler';
import {User} from '~/app/shared/user/user.model';
import {Injectable} from '@angular/core';


@Injectable()
export class UserService {
    register(user: User) {
        alert('About to register: ' + user.email);
    }
}
