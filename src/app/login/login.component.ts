import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {UserService} from '~/app/shared/user/user.service';

@Component({
    selector: 'app-login',
    providers: [UserService],
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
    title = 'Logowanie';
    isLoggingIn = true;

    signupForm: FormGroup;

    constructor() {
    }

    ngOnInit() {
        this.signupForm = new FormGroup({
            'email': new FormControl(null),
            'password': new FormControl(null)
        });
    }

    submit() {
        alert(this.signupForm.controls.e);
    }

    toggleDisplay() {
        this.isLoggingIn = !this.isLoggingIn;
    }


}
